const puppeteer = require('puppeteer');
const config = require('./config');
const credenciales = require('./credenciales');

/*Argumentos*/

process.argv.forEach(function (val, index, array) { console.log(index + ': ' + val); });

let checktime = process.argv[2] || '';
let checktype = process.argv[3] || '';
let id_reason = process.argv[4] || '';

let parseData;

console.log('credenciales: ', credenciales);

async function main() {


    /*Comprobamos checktype 0=entrada 1=salida*/
    if (checktype != '') {

        parseData = checktype.toLowerCase();

        if (parseData == config.enter || parseData == '0') {

            checktype = 0;

        }

        if (parseData == config.out || parseData == '1') {

            checktype = 1;

        }


    }

    console.log('>> En Proceso');

    const browser = await puppeteer.launch({headless: true, args: ['--no-sandbox']});
    const page = await browser.newPage();

    await page.setViewport({width: 1200, height: 720});


    await page.goto(config.loginWeb, {waitUntil: 'networkidle0'}); // wait until page load


    await Promise.all([
        page.click('.login_box_type a:last-child'),
        page.waitForNavigation({waitUntil: 'networkidle0'}),

    ]);


    await Promise.all([
        page.type('#id_empPwdTip', credenciales.pass)

    ]);


    await Promise.all([


        page.type('#id_empNameTip', credenciales.user)

    ]);


    // click and wait for navigation
    await Promise.all([
        page.click('#id_empLogin'),
        page.waitForNavigation({waitUntil: 'networkidle0'}),
    ]);


    //2
    await page.goto(config.checkWeb, {waitUntil: 'networkidle0'}); // wait until page load


    console.log('>> Insertar: ', checktime + ' ' + checktype + ' ' + id_reason);
    await page.evaluate((checktime, checktype, id_reason) => {

        /*Hora de entrada*/
        document.querySelector('#id_checktime').value = String(checktime),
            document.querySelector('#id_checktype').value = String(checktype),
            document.querySelector('#id_reason').value = String(id_reason),
            document.querySelector('.lineH22 a').click()


    }, checktime, checktype, id_reason);

    ///await page.screenshot({path: 'screenshot.png'});

    browser.close();

    console.log('>> Proceso finalizado..')


}

main();




